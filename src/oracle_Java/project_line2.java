package oracle_Java;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.sql.SQLException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

// 전년 대비 신규 가입자 증감률 그래프

public class project_line2 extends ApplicationFrame {

	public project_line2(String title2) throws Exception {
		super(title2);
		final CategoryDataset dataset2 = createDataset();
		final JFreeChart chart2 = createChart(dataset2);
		final ChartPanel chartPanel2 = new ChartPanel(chart2);
		chartPanel2.setPreferredSize(new Dimension(500, 270));
		setContentPane(chartPanel2);
	}

	private CategoryDataset createDataset() throws SQLException {

		// row key
		final String city1 = "서울";
		final String city2 = "부산";
		final String city3 = "대구";
		final String city4 = "인천";
		final String city5 = "광주";
		final String city6 = "대전";
		final String city7 = "울산";
		// column key
		final String year1 = "2007";
		final String year2 = "2008";
		final String year3 = "2009";
		final String year4 = "2010";
		final String year5 = "2011";
		final String year6 = "2012";
		final String year7 = "2013";
		final String year8 = "2014";
		final String year9 = "2015";
		final String year10 = "2016";
		final String year11 = "2017";

		//dataset 설정하여 데이터 입력
		final DefaultCategoryDataset dataset2 = new DefaultCategoryDataset();
		// 서울
		dataset2.addValue(427, city1, year2);
		dataset2.addValue(0.5, city1, year3);
		dataset2.addValue(0, city1, year4);
		dataset2.addValue(-0.2, city1, year5);
		dataset2.addValue(0.1, city1, year6);
		dataset2.addValue(-0.8, city1, year7);
		dataset2.addValue(0.6, city1, year8);
		dataset2.addValue(-0.7, city1, year9);
		dataset2.addValue(0.5, city1, year10);
		dataset2.addValue(-20, city1, year11);
		// 부산
		dataset2.addValue(427.9, city2, year2);
		dataset2.addValue(-0.4, city2, year3);
		dataset2.addValue(0.3, city2, year4);
		dataset2.addValue(-0.4, city2, year5);
		dataset2.addValue(0.7, city2, year6);
		dataset2.addValue(-1, city2, year7);
		dataset2.addValue(0.6, city2, year8);
		dataset2.addValue(0.8, city2, year9);
		dataset2.addValue(-1.5, city2, year10);
		dataset2.addValue(-19.4, city2, year11);
		// 대구
		dataset2.addValue(428.4, city3, year2);
		dataset2.addValue(-0.9, city3, year3);
		dataset2.addValue(-1.1, city3, year4);
		dataset2.addValue(1.9, city3, year5);
		dataset2.addValue(-1, city3, year6);
		dataset2.addValue(0.1, city3, year7);
		dataset2.addValue(1.4, city3, year8);
		dataset2.addValue(-0.6, city3, year9);
		dataset2.addValue(0.7, city3, year10);
		dataset2.addValue(-20.7, city3, year11);
		// 인천
		dataset2.addValue(420.3, city4, year2);
		dataset2.addValue(-1.1, city4, year3);
		dataset2.addValue(0.4, city4, year4);
		dataset2.addValue(2, city4, year5);
		dataset2.addValue(-1.4, city4, year6);
		dataset2.addValue(-0.3, city4, year7);
		dataset2.addValue(2.4, city4, year8);
		dataset2.addValue(-0.4, city4, year9);
		dataset2.addValue(-1.1, city4, year10);
		dataset2.addValue(-20.6, city4, year11);
		// 광주
		dataset2.addValue(435.4, city5, year2);
		dataset2.addValue(0.1, city5, year3);
		dataset2.addValue(-0.2, city5, year4);
		dataset2.addValue(0.9, city5, year5);
		dataset2.addValue(0, city5, year6);
		dataset2.addValue(-0.7, city5, year7);
		dataset2.addValue(-0.6, city5, year8);
		dataset2.addValue(-0.4, city5, year9);
		dataset2.addValue(1.2, city5, year10);
		dataset2.addValue(-20.1, city5, year11);
		// 대전
		dataset2.addValue(432.4, city6, year2);
		dataset2.addValue(-0.2, city6, year3);
		dataset2.addValue(1, city6, year4);
		dataset2.addValue(-1.5, city6, year5);
		dataset2.addValue(2.7, city6, year6);
		dataset2.addValue(-3.6, city6, year7);
		dataset2.addValue(1, city6, year8);
		dataset2.addValue(1, city6, year9);
		dataset2.addValue(-0.9, city6, year10);
		dataset2.addValue(-19.6, city6, year11);
		// 울산
		dataset2.addValue(404.6, city7, year2);
		dataset2.addValue(0.6, city7, year3);
		dataset2.addValue(-3.4, city7, year4);
		dataset2.addValue(4.2, city7, year5);
		dataset2.addValue(0.4, city7, year6);
		dataset2.addValue(-0.7, city7, year7);
		dataset2.addValue(-2, city7, year8);
		dataset2.addValue(0.5, city7, year9);
		dataset2.addValue(-0.1, city7, year10);
		dataset2.addValue(-17.9, city7, year11);

		return dataset2;

	}

	// 4. chart 메소드
	private JFreeChart createChart(final CategoryDataset dataset2) throws Exception {
		//1) 차트 생성
		final JFreeChart chart2 = ChartFactory.createLineChart("전년도 대비 가입자 증감률", "년도", "증감률", dataset2,
				PlotOrientation.VERTICAL, true, true, false);

		// 2) 포맷 및 한글화
		CategoryPlot plot2 = (CategoryPlot) chart2.getPlot();
		plot2.setBackgroundPaint(Color.white);
		plot2.setRangeGridlinePaint(Color.lightGray);
		plot2.getDomainAxis().setLabelFont(new Font("굴림", Font.BOLD, 13));
		plot2.getDomainAxis().setTickLabelFont(new Font("굴림", Font.BOLD, 8));
		plot2.getRangeAxis().setLabelFont(new Font("굴림", Font.BOLD, 13));
		plot2.getRangeAxis().setTickLabelFont(new Font("굴림", Font.BOLD, 10));
		chart2.getLegend().setItemFont(new Font("굴림", Font.PLAIN, 13));

		TextTitle chartTitle = new TextTitle("전년도 대비 가입자 증감률", new Font("나눔바른고딕", Font.BOLD, 20));

		chart2.setTitle(chartTitle);
		System.out.println("한글 깨짐 해결!");
		// 3) png로 저장
		ChartUtilities.saveChartAsPNG(new File("C:\\Users\\남세라\\Desktop\\visualisation2.png"), chart2, 600, 300);

		return chart2;
	}

	public static void main(final String[] args) throws Exception {

		final project_line2 lineGraph2 = new project_line2("전년도 대비 가입자 증감률");
		lineGraph2.pack();
		RefineryUtilities.centerFrameOnScreen(lineGraph2);
		lineGraph2.setVisible(true);

	}

}
