package oracle_Java;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

//연도 별 신규 가입자 수 그래프

public class project_line extends ApplicationFrame {

	public project_line(String title) throws Exception {
		super(title);
		final CategoryDataset dataset = createDataset();
		final JFreeChart chart = createChart(dataset);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(500, 270));
		setContentPane(chartPanel);
	}

	private CategoryDataset createDataset() throws SQLException {

		// 1. DB로 연결
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@192.168.26.72:1521/dcswCDB";
		// 계정
		String user = "dcsw05";
		String password = "CD05";

		try {
			Class.forName(driver);
			System.out.println("jdbc driver 로딩 성공");
			DriverManager.getConnection(url, user, password);
			System.out.println("오라클 연결 성공");
		} catch (ClassNotFoundException e) {
			System.out.println("jdbc driver 로딩 실패");
		} catch (SQLException e) {
			System.out.println("오라클 연결 실패");
		}
		// Connection 객체 생성, 192.169.23.18 : 오라클 접속 주소
		// 1521 : 오라클과 연결한 포트번호를 포워딩한 포트번호
		Connection conn = DriverManager.getConnection(url, user, password);
		// 2. 쿼리
		// 쿼리를 보내기 위한 Statement 객체 생성
		Statement stmt = conn.createStatement();
		// 쿼리 결과를 받기 위한 ResultSet 객체 생성 및 쿼리결과 할당
		// 쿼리문 실행
		ResultSet rset = stmt.executeQuery("\r\n"
				+ "SELECT * from (select TO_CHAR(ENROLL_DT,'YYYY') \"년도\", COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '서울',1)) \"서울\",\r\n"
				+ "        COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '부산',1)) \"부산\", COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '대구',1)) \"대구\",\r\n"
				+ "        COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '인천',1, 'uC', 1)) \"인천\", COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '광주',1)) \"광주\",\r\n"
				+ "        COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '대전',1)) \"대전\", \r\n"
				+ "         COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '울산',1)) \"울산\"\r\n"
				+ "FROM CUSTOMER GROUP BY TO_CHAR(ENROLL_DT,'YYYY') ORDER BY TO_CHAR(ENROLL_DT,'YYYY'))");

		// 3. 데이터셋
		// 1) dataset 인스턴스화
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		// 3. while문으로 차례로 데이터 입력
		while (rset.next()) {
			dataset.addValue(Double.parseDouble(rset.getString("서울")), "서울", rset.getString("년도"));
			dataset.addValue(Double.parseDouble(rset.getString("부산")), "부산", rset.getString("년도"));
			dataset.addValue(Double.parseDouble(rset.getString("대구")), "대구", rset.getString("년도"));
			dataset.addValue(Double.parseDouble(rset.getString("인천")), "인천", rset.getString("년도"));
			dataset.addValue(Double.parseDouble(rset.getString("광주")), "광주", rset.getString("년도"));
			dataset.addValue(Double.parseDouble(rset.getString("대전")), "대전", rset.getString("년도"));
			dataset.addValue(Double.parseDouble(rset.getString("울산")), "울산", rset.getString("년도"));
		}
		return dataset;

	}

	// 4. chart 메소드
	private JFreeChart createChart(final CategoryDataset dataset) throws Exception {
		// 차트 생성
		final JFreeChart chart = ChartFactory.createLineChart("년도 별 신규 가입자 수", "년도", "가입자 수", dataset,
				PlotOrientation.VERTICAL, true, true, false);

		// 1) 차트 포맷
		// 2) 한글화
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.white);
		plot.getDomainAxis().setLabelFont(new Font("굴림", Font.BOLD, 13));
		plot.getDomainAxis().setTickLabelFont(new Font("굴림", Font.BOLD, 8));
		plot.getRangeAxis().setLabelFont(new Font("굴림", Font.BOLD, 13));
		plot.getRangeAxis().setTickLabelFont(new Font("굴림", Font.BOLD, 8));
		chart.getLegend().setItemFont(new Font("굴림", Font.PLAIN, 10));

		TextTitle chartTitle = new TextTitle("년도 별 신규 가입자 수", new Font("나눔바른고딕", Font.BOLD, 20));

		chart.setTitle(chartTitle);
		System.out.println("한글 깨짐 해결!");
		// 3) png로 저장
		ChartUtilities.saveChartAsPNG(new File("C:\\Users\\남세라\\Desktop\\visualisation.png"), chart, 600, 300);

		return chart;
	}

	public static void main(final String[] args) throws Exception {

		final project_line lineGraph = new project_line("년도 별 신규 가입자 수");
		lineGraph.pack();
		RefineryUtilities.centerFrameOnScreen(lineGraph);
		lineGraph.setVisible(true);

	}

}
