package oracle_Java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class project {

	public static void main(String[] args) throws SQLException {

	

		// 1. 연결
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@192.168.26.72:1521/dcswCDB";

		String user = "dcsw05";
		String password = "CD05";

		try {
			Class.forName(driver);
			System.out.println("jdbc driver 로딩 성공");
			DriverManager.getConnection(url, user, password);
			System.out.println("오라클 연결 성공");
		} catch (ClassNotFoundException e) {
			System.out.println("jdbc driver 로딩 실패");
		} catch (SQLException e) {
			System.out.println("오라클 연결 실패");
		}
		// Connection 객체 생성, 192.169.23.18 : 오라클 접속 주소
		// 1521 : 오라클과 연결한 포트번호를 포워딩한 포트번호
		Connection conn = DriverManager.getConnection(url, user, password);
		// 쿼리를 보내기 위한 Statement 객체 생성
		Statement stmt = conn.createStatement();
		// 쿼리 결과를 받기 위한 ResultSet 객체 생성 및 쿼리결과 할당

		ResultSet rset = stmt.executeQuery("\r\n"
				+ "SELECT * from (select TO_CHAR(ENROLL_DT,'YYYY') \"년도\", COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '서울',1)) \"서울\",\r\n"
				+ "        COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '부산',1)) \"부산\", COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '대구',1)) \"대구\",\r\n"
				+ "        COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '인천',1, 'uC', 1)) \"인천\", COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '광주',1)) \"광주\",\r\n"
				+ "        COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '대전',1)) \"대전\", \r\n"
				+ "         COUNT(DECODE(SUBSTR(ADDRESS1, 1,2), '울산',1)) \"울산\"\r\n"
				+ "FROM CUSTOMER GROUP BY TO_CHAR(ENROLL_DT,'YYYY') ORDER BY TO_CHAR(ENROLL_DT,'YYYY'))");

		
		  ResultSetMetaData rsmd = rset.getMetaData();
		 
		  String columnName1 = rsmd.getColumnName(1); String columnName2 =
		  rsmd.getColumnName(2); String columnName3 = rsmd.getColumnName(3); String
		  columnName4 = rsmd.getColumnName(4); String columnName5 =
		  rsmd.getColumnName(5); String columnName6 = rsmd.getColumnName(6);
		  System.out.println(" ");
		  System.out.println(columnName1+" "+columnName2+" "+columnName3+" "
		  +columnName4+" "+columnName5+" "+columnName6);
		  System.out.println("--------------------------------");
		  
		  while(rset.next()) {
		  
		  System.out.println(rset.getInt(1)+" "+rset.getInt(2)+" "+rset.getInt(3)+" "
		  +rset.getInt(4)+" "+rset.getInt(5)+" "+rset.getInt(6));
		  
		  }
	}
}
